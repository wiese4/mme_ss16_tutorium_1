/* globals App */
App.Controller = function () {
    "use strict";

    var that = {},
        list,
        button,
        addCallback,
        removeFromListCallback;

    function init(options) {
        // DOM representations of the UI elements
        list = options.list;
        button = options.button;

        registerListeners();

        return that;
    }

    function setAddCallback(callback) {
        // here we set the addCallback
        // notice that the controller doesn't know, what actually happens in the callback
        // and shouldn't know anyway!
        addCallback = callback;
    }

    function setRemoveFromListCallback(callback) {
        // here we set the removeFromListCallback
        // notice that the controller doesn't know, what actually happens in the callback
        // and shouldn't know anyway!
        removeFromListCallback = callback;
    }

    function onListClicked(event) {
        // was it a click on a list-item?
        if (event.target.className === "list-item") {
            // trigger the callback
            // the controller still has no idea what the callback actually does
            removeFromListCallback(event.target.id.split("-")[1]);
        }
    }

    function onButtonClicked() {
        // trigger the callback
        // the controller still has no idea what the callback actually does
        addCallback();
    }

    function registerListeners() {
        // handle the actual user interactions
        list.addEventListener("click", onListClicked);
        button.addEventListener("click", onButtonClicked);
    }

    that.init = init;
    that.setAddCallback = setAddCallback;
    that.setRemoveFromListCallback = setRemoveFromListCallback;
    return that;
};
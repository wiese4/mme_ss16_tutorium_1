var App = (function () {
    "use strict";

    var that = {},
        model,
        controller,
        listView;

    function init() {
        initModel();
        initController();
        initListView();
    }

    function onItemAdded() {
        // since app.js knows all other modules, it can handle the communication between them
        // so this is the callback called when an item is added (by clicking the button)
        // we first add the item to the model
        var item = model.addToList();
        // and then pass the new item to the listView to be rendered
        listView.addToList(item);
    }

    function onItemRemoved(id) {
        // since app.js knows all other modules, it can handle the communication between them
        // so this is the callback called when an item is removed (by clicking a list-item)
        // we first remove the item from the model
        var item = model.removeFromList(id);
        // and then pass the removed item to the listView to be deleted from the UI
        listView.removeFromList(item);
    }

    function initController() {
        // the controller handles the user interactions
        // hence it gets access to the DOM representations of UI elements the user can interact with
        controller = App.Controller().init({
            button: document.getElementById("add-button"),
            list: document.getElementById("list")
        });

        // information hiding! each module should only know enough of others to do its job
        // the controller doesn't need to know more than the two callbacks
        controller.setAddCallback(onItemAdded);
        controller.setRemoveFromListCallback(onItemRemoved);
    }

    function initListView() {
        listView = App.ListView().init(document.getElementById("list"));
    }

    function initModel() {
        model = App.Model().init();
    }

    that.init = init;
    return that;
}());
/* globals App */
// the model mustn't know anything about the UI!
// so there is no place here for document.get... or such things
App.Model = function () {
    "use strict";

    var that = {},
        list = [],
        lastId = 1;

    function addToList() {
        // create new item with random data
        var obj = {
            id: lastId,
            value: Math.ceil(Math.random() * 100)
        };

        // add the new item to the list
        list.push(obj);
        lastId++;

        // return the newly created item
        return obj;
    }

    function removeFromList(id) {
        var res;

        // find the index of the removed item in the list
        list.forEach(function (obj, ind) {
            if (obj.id === parseInt(id)) {
                res = ind;
            }
        });

        // remove the item from the list and return the removed item
        return list.splice(res, 1)[0];
    }

    function init() {
        return that;
    }

    that.init = init;
    that.addToList = addToList;
    that.removeFromList = removeFromList;

    return that;
};
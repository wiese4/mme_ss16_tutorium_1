/* globals App */
App.ListView = function () {
    "use strict";

    var that = {},
        list;

    function init(l) {
        // DOM representation of the list
        list = l;

        return that;
    }

    function removeFromList(obj) {
        // find and remove the list-item
        var item = list.querySelector("#item-" + obj.id);
        list.removeChild(item);
    }

    function addToList(obj) {
        // create a new list-item
        var el = document.createElement("li");

        // add some data
        el.id = "item-" + obj.id;
        el.className = "list-item";
        el.innerHTML = obj.value;

        // append the item to the actual HTML
        list.appendChild(el);
    }

    that.init = init;
    that.removeFromList = removeFromList;
    that.addToList = addToList;
    return that;
};